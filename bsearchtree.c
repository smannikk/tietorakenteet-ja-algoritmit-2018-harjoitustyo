#include <stdio.h>
#include <stdlib.h>
#include "bsearchtree.h"

void freeNodes(word *node);
void nodeToArray(word *node);

wordTree createWordTree()
{
    wordTree tree;
    tree.root = NULL;
    tree.nodes = 0;
    tree.wordsTotal = 0;
    return tree;
}

void wordInit(word *wrd, string *key)
{
    wrd->key = *key;
    wrd->up = NULL;
    wrd->left = NULL;
    wrd->right = NULL;
    wrd->occurrences = 0;
}

void wordDeInit(word *wrd)
{
    stringDeInit(&wrd->key);
    wrd->left = NULL;
    wrd->right = NULL;
    wrd->occurrences = 0;
}

word *wordTreeFind(wordTree *tree, string *key)
{
    int comp;
    word *node = tree->root;

    while (node != NULL && (comp = stringCompare(key, &node->key)) != 0)
        node = comp > 0 ? node->right : node->left;

    return node;
}

word *wordTreeAdd(wordTree *tree, string *key)
{
    word *nodeY = NULL, *nodeX = tree->root;

    while (nodeX != NULL)
    {
        nodeY = nodeX;
        int comp = stringCompare(key, &nodeX->key);

        if (comp > 0)
            nodeX = nodeX->right;
        else if (comp < 0)
            nodeX = nodeX->left;
        else return nodeX;
    }

    word *newWrd = malloc(sizeof(word));

    if (newWrd == NULL)
        return NULL;

    wordInit(newWrd, key);

    newWrd->up = nodeY;
    if (nodeY == NULL)
        tree->root = newWrd;
    else if (stringCompare(key, &nodeY->key) > 0)
        nodeY->right = newWrd;
    else
        nodeY->left = newWrd;

    ++tree->nodes;

    return newWrd;
}

void wordTreeDestruct(wordTree *tree)
{
    freeNodes(tree->root);
    tree->root = NULL;
}

void freeNodes(word *node)
{
    if (node == NULL)
        return;

    freeNodes(node->left);
    freeNodes(node->right);

    wordDeInit(node);
    free(node);
}

word **g_arrayP;

word **wordTreeToArray(wordTree *tree)
{
    word **array = malloc(sizeof(word*) * tree->nodes);
    if (array == NULL)
        return NULL;

    g_arrayP = array;

    nodeToArray(tree->root);

    return array;
}

void nodeToArray(word *node)
{
    if (node == NULL)
        return;

    *g_arrayP++ = node;
    nodeToArray(node->left);
    nodeToArray(node->right);
}
