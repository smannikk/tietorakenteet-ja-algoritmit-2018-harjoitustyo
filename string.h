#ifndef F_STRING_H
#define F_STRING_H

// A dynamic-size null-terminated string type.
typedef struct
{
    char *data;
    int reserved, length;
} string;

// Constructor
void stringInit(string *str);

// Destructor
void stringDeInit(string *str);

// Set the content of the string.
// The passed char array is copied.
char *stringSet(string *str, char *newStr);

// Concatenate a single character to
// the end of the string.
char *stringAddChar(string *str, char ch);

// Reallocate memory for the string.
// Mainly for internal use.
char *stringRealloc(string *str, unsigned reserve);

// String comparison (ordered by char code points)
// Returns 0 if the strings are equal,
// A positive value if str1 > str2
// A negative value if str1 < str2
int stringCompare(string *str1, string *str2);

#endif
