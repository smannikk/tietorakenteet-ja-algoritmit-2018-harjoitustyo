#ifndef F_BSEARCHTREE_H
#define F_BSEARCHTREE_H

#include "string.h"

// A node for the word tree.
typedef struct t_word
{
    struct t_word *up;
    struct t_word *left;
    struct t_word *right;

    // Let's use a dynamically allocated string instead of assuming a maximum length for a word.
    // Someone might want to try this code on a book written by someone like James Joyce:
    // "bababadalgharaghtakamminarronnkonnbronntonnerronntuonnthunntrovarrhounawnskawntoohoohoordenenthurnuk"
    string key;

    int occurrences;
} word;

typedef struct
{
    word *root;
    int nodes, wordsTotal;
} wordTree;

// Use this to create and initialize
// a new tree.
wordTree createWordTree();

// Free all the nodes in the tree.
void wordTreeDestruct(wordTree *tree);

// Constructor for a single node.
// The key string is moved, not copied.
// Don't pass a string you need later.
void wordInit(word *wrd, string *key);

// Destructor for a single node.
void wordDeInit(word *wrd);

// Finds a requested word from the tree.
// Returns NULL if the word was not found.
word *wordTreeFind(wordTree *tree, string *key);

// Adds a word to the tree. Returns a
// pointer to the new node (or to an
// old one if it already exists).
// The key string is moved, not copied.
// Don't pass a string you need later.
word *wordTreeAdd(wordTree *tree, string *key);

// ----------------------------------------------
// Removal of single nodes is not needed by this
// program, so it is left unimplemented.
// ----------------------------------------------

// Creates an array containing pointers to each
// node of the tree. Returns a pointer to the
// array. The length of the array will be equal
// to the 'nodes' member variable of the tree.
word **wordTreeToArray(wordTree *tree);

#endif
