#include <stdio.h>
#include <stdlib.h>
#include "sort.h"

inline void swap(word **array, int i, int j);
inline void swapInt(int *array, int i, int j);
void quickSort(word **array, int first, int last);
int partition(word **array, int first, int last);

// The recursive version overflows the call stack when
// the amount of distinct words is very high, so an iterative
// version needs to be used instead.
void iterativeQuickSort(word **array, int n);

int findNMostCommonWords(wordTree *tree, word **out, int n)
{
    word **array = wordTreeToArray(tree);
    if (array == NULL || tree->nodes <= 0)
        return 0;

    //quickSort(array, 0, tree->nodes-1);
    iterativeQuickSort(array, tree->nodes);

    int i;
    for (i=0; i<tree->nodes && i<n; ++i)
        out[i] = array[i];

    free(array);

    return i;
}

void quickSort(word **array, int first, int last)
{
    if (first < last)
    {
        int middle = partition(array, first, last);
        quickSort(array, first, middle-1);
        quickSort(array, middle+1, last);
    }
}

int partition(word **array, int first, int last)
{
    int pivot = array[last]->occurrences;

    int j, i = first-1;

    for (j=first; j<last; ++j)
        if (array[j]->occurrences >= pivot)
            swap(array, ++i, j);

    swap(array, ++i, last);

    return i;
}

void swap(word **array, int i, int j)
{
    word *t = array[i];
    array[i] = array[j];
    array[j] = t;
}

// Following this example: http://alienryderflex.com/quicksort/
// This should not fail for inputs where n < 2^300, in other
// words, it should never fail in practice.
void iterativeQuickSort(word **array, int n)
{
    // Still allocating data in the stack, but there's much
    // less overhead with an iterative implementation.
    const int maxDepth = 300;
    int begin[maxDepth], end[maxDepth];
    int first, last, i;
    word *pivot;

    begin[0] = 0;
    end[0] = n;

    for (i=0; i>=0;)
    {
        first = begin[i];
        last = end[i]-1;

        if (first < last)
        {
            pivot = array[first];
            while (first < last)
            {
                while (array[last]->occurrences <= pivot->occurrences && first < last)
                    --last;
                if (first < last)
                    array[first++] = array[last];

                while (array[first]->occurrences >= pivot->occurrences && first < last)
                    ++first;
                if (first < last)
                    array[last--] = array[first];
            }

            array[first] = pivot;
            begin[++i] = first+1;
            end[i] = end[i-1];
            end[i-1] = first;

            if (end[i] - begin[i] > end[i-1] - begin[i-1])
            {
                swapInt(begin, i, i-1);
                swapInt(end, i, i-1);
            }
        }
        else
            --i;
    }
}

void swapInt(int *array, int i, int j)
{
    int t = array[i];
    array[i] = array[j];
    array[j] = t;
}
