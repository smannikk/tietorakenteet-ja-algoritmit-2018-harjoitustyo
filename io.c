#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "io.h"

const char g_specialCharacters[] = {'�', '�', '�', '�', '�', '�', '�', '�'};
const char g_specialCharactersUpper[] = {'�', '�', '�', '�', '�', '�', '�', '�'};

// The function assumes that the letters
// have contiguous code points. Works for
// most encodings, and, if I remember correctly,
// the C standard requires it from the compiler.
int isPartOfWord(char ch)
{
    if (ch >= 'A' && ch <= 'Z')
        return 1;

    if (ch >= 'a' && ch <= 'z')
        return 1;

    if (ch > 128 || ch < 0)
    {
        int i;
        for (i=0; i<sizeof(g_specialCharacters)/sizeof(char); ++i)
            if (ch == g_specialCharacters[i])
                return 1;
    }

    return ch == '\'';
}

string getWord(FILE *file, int *reachedEOF)
{
    char ch;
    string str;
    stringInit(&str);

    if (stringRealloc(&str, 16) == NULL)
        return str;

    if (reachedEOF != NULL)
        *reachedEOF = 0;

    while ((ch = fgetc(file)) != EOF && !isPartOfWord(ch));

    if (ch == EOF)
    {
        if (reachedEOF != NULL)
            *reachedEOF = 1;
        return str;
    }

    do
    {
        if (stringAddChar(&str, toUppercase(ch)) == NULL)
            return str;

        ch = getc(file);
    } while (ch != EOF && isPartOfWord(ch));

    if (ch == EOF && reachedEOF != NULL)
        *reachedEOF = 1;

    return str;
}

wordTree countWordOccurrences(FILE *file)
{
    wordTree tree = createWordTree();

    for (;;)
    {
        int reachedEOF;
        string str = getWord(file, &reachedEOF);

        if (str.length > 0)
        {
            word *wrd = wordTreeAdd(&tree, &str);
            if (wrd != NULL)
            {
                ++wrd->occurrences;
                ++tree.wordsTotal;
            }
        }

        if (reachedEOF)
            break;
    }

    return tree;
}

FILE *askForFile(const char *openType)
{
    char fname[128];
    printf("Enter file path: ");
    while (fgets(fname, 128, stdin) == NULL);

    int len = strlen(fname);
    if (len > 0 && fname[len-1] == '\n')
        fname[len-1] = '\0';

    FILE *file = fopen(fname, openType);

    if (file == NULL)
        printf("\nThe file could not be opened.");

    return file;
}

void outputResults(wordTree *tree, word **array, int length, FILE *stream)
{
    int i;
    fprintf(stream, "# Number of words in total: %d\n# Number of distinct words: %d\n",
           tree->wordsTotal, tree->nodes);
    fputs("\n# Most common words:\n\n# WORD NUMBER_OF_OCCURRENCES\n", stream);
    for (i=0; i<length; ++i)
        fprintf(stream, "%s %d\n", array[i]->key.data, array[i]->occurrences);
}

char toUppercase(char ch)
{
    if (ch > 128 || ch < 0)
    {
        int i;
        for (i=0; i<sizeof(g_specialCharacters)/sizeof(char); ++i)
            if (ch == g_specialCharacters[i])
                return g_specialCharactersUpper[i];
    }

    return toupper(ch);
}
