#ifndef F_SORT_H
#define F_SORT_H

#include "bsearchtree.h"

// Finds the N most common words in the tree and places
// pointers to them in an array in descending order of
// commonness. Returns the number of words added to the
// array.
int findNMostCommonWords(wordTree *tree, word **out, int n);

#endif
