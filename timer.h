#ifndef F_TIMER_H
#define F_TIMER_H

#include <time.h>

typedef struct
{
    clock_t clock;
} timer;

void startTimer(timer *t);

// Time elapsed since starting the timer in milliseconds.
int timeElapsed(timer *t);

#endif
