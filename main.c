/*
    Oulun yliopisto
    Tietorakenteet ja algoritmit 2017-2018
    C-harjoitustyö
    Sami Männikkö

    A program that counts the occurrences of individual words
    in a text file and finds the one hundred most common ones.

    The words are first read from the file into a binary search
    tree, counting the number of their occurrences along the
    way. Then pointers to the nodes of the tree are placed into
    an array, which is then sorted by the number of occurrences
    of each word in decreasing order using quicksort.

    To execute from the command line:
    WordCounter <input file (optional)> <output file (optional)>
*/

#include <stdio.h>
#include <stdlib.h>

#include "bsearchtree.h"
#include "sort.h"
#include "io.h"
#include "timer.h"

int main(int argc, char *argv[])
{
    FILE *file = NULL;
    if (argc > 1)
    {
        file = fopen(argv[1], "r");
    }
    else
    {
        file = askForFile("r");
        puts("");
    }

    const int interactive = argc < 3;

    if (file == NULL)
    {
        if (interactive)
        {
            puts("\nPress ENTER to exit.");
            getchar();
        }
        return 1;
    }

    timer t;
    startTimer(&t);
    puts("Processing...");

    wordTree tree = countWordOccurrences(file);
    fclose(file);

    const int numWords = 100;
    word *mostCommonWords[numWords];
    int wordsFound = findNMostCommonWords(&tree, mostCommonWords, numWords);

    int elapsed = timeElapsed(&t);
    printf("\nProcessing finished in %.3f seconds.\n\n", elapsed / 1000.0);

    FILE *output = NULL;
    if (output == NULL)
    {
        if (argc > 2)
        {
            output = fopen(argv[2], "w");
        }
        else
        {
            output = stdout;
            if (interactive)
            {
                printf("Would you like to save the results to a file? (y/n)\n\n");
                char ch = getchar();
                while (getchar() != '\n');
                if (ch == 'y' || ch == 'Y')
                {
                    puts("");
                    output = askForFile("w");
                }
                else
                    puts("");
            }
        }
    }

    if (output == NULL)
    {
        puts("\nError: Cannot output to specified file.\n");
        output = stdout;
    }

    outputResults(&tree, mostCommonWords, wordsFound, output);

    if (output != stdout)
    {
        if (fclose(output) == 0)
            puts("\nOutput was saved to the specified file.");
        else
            puts("\nError: Saving output to the specified file failed.");
    }

    wordTreeDestruct(&tree);

    if (interactive)
    {
        puts("\nPress ENTER to exit.");
        getchar();
    }

    return 0;
}
