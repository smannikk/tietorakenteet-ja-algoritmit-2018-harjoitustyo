#ifndef F_IO_H
#define F_IO_H

#include <stdio.h>
#include "string.h"
#include "bsearchtree.h"

// Returns a string containing a word in uppercase
// from a stream or a file. The length of the returned
// word should always be strictly positive, unless an
// EOF was reached or there was an error.
string getWord(FILE *file, int *reachedEOF);

// Check if a char is one of {a-z, A-Z, '}
int isPartOfWord(char ch);

// Counts the occurrences of each individual word
// and places the words and the values in a binary
// search tree.
wordTree countWordOccurrences(FILE *file);

// Outputs the results to a stream/file.
void outputResults(wordTree *tree, word **array, int length, FILE *stream);

// Asks the user for an input file.
FILE *askForFile(const char *openType);

// Wrapper for toupper() that takes some special
// characters into account also.
char toUppercase(char ch);

#endif
