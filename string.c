#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "string.h"

void stringInit(string *str)
{
    str->data = NULL;
    str->reserved = 0;
    str->length = 0;
}

void stringDeInit(string *str)
{
    free(str->data);
    stringInit(str);
}

char *stringSet(string *str, char *newStr)
{
    unsigned length = strlen(newStr);

    if (length >= str->reserved)
    {
        free(str->data);
        str->data = NULL;
        stringRealloc(str, length+1);
    }

    if (str->data == NULL)
        return NULL;

    return strcpy(str->data, newStr);
}

char *stringAddChar(string *str, char ch)
{
    if (str->length+1 >= str->reserved)
    {
        stringRealloc(str, str->reserved+8);
        if (str->data == NULL)
            return NULL;
    }

    str->data[str->length] = ch;
    str->data[++str->length] = '\0';

    return str->data;
}

char *stringRealloc(string *str, unsigned reserve)
{
    char *oldData = str->data;
    char *newData = malloc(sizeof(char) * reserve);

    if (newData == NULL)
        return NULL;

    if (oldData)
    {
        int i;
        for (i=0; i+1<reserve && oldData[i]!='\0'; ++i)
            newData[i] = oldData[i];

        if (reserve > 0)
            newData[i] = '\0';
        str->length = i;

        free(oldData);
    }
    else if (reserve > 0)
    {
        newData[0] = '\0';
        str->length = 0;
    }

    str->reserved = reserve;
    str->data = newData;

    return str->data;
}

// strcmp() could do the work, but
// let's write our own function anyway.
int stringCompare(string *str1, string *str2)
{
    const char *s1 = str1->data;
    const char *s2 = str2->data;

    for (; *s1 && (*s1 == *s2); ++s1, ++s2);

    return *(const unsigned char*)s1 - *(const unsigned char*)s2;
}
