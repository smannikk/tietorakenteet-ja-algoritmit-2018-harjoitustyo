#include "timer.h"

void startTimer(timer *t)
{
    t->clock = clock();
}

int timeElapsed(timer *t)
{
    clock_t diff = clock() - t->clock;
    return diff*1000 / CLOCKS_PER_SEC;
}
